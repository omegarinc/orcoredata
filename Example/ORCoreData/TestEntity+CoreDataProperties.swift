//
//  TestEntity+CoreDataProperties.swift
//  ORCoreData
//
//  Created by Maxim Soloviev on 08/04/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension TestEntity {

    @NSManaged var uid: String?

}
